#!/usr/bin/env python2

import sys
import lxml.etree

links = {}
nodes = {}

f = open(sys.argv[1],'r')
contents = f.read()
f.close()
root = lxml.etree.fromstring(contents)

mycluster = None
if len(sys.argv) > 2:
    mycluster = sys.argv[2]
else:
    sys.exit(1)

# Find all the link roles:
for elm in root.getchildren():
    if not elm.tag.endswith("}link"):
        continue
    name = elm.get("client_id")
    cluster = None
    role = None
    for elm2 in elm.getchildren():
        if elm2.tag.endswith("}label") and elm2.get("name") == "cluster":
            cluster = elm2.text
        if elm2.tag.endswith("}label") and elm2.get("name") == "role":
            role = elm2.text
    if mycluster == cluster and role:
        if not role in links:
            links[role] = []
        links[role].append(name)

# Find all the node roles:
for elm in root.getchildren():
    if not elm.tag.endswith("}node"):
        continue
    name = elm.get("client_id")
    cluster = None
    role = None
    for elm2 in elm.getchildren():
        if elm2.tag.endswith("}label") and elm2.get("name") == "cluster":
            cluster = elm2.text
        if elm2.tag.endswith("}label") and elm2.get("name") == "role":
            role = elm2.text
    if mycluster == cluster and role:
        if not role in nodes:
            nodes[role] = []
        nodes[role].append(name)

for role in nodes:
    print "%s=\"%s\"" % (role.upper()," ".join(nodes[role]))
for role in links:
    print "%s=\"%s\"" % (role.upper()," ".join(links[role]))

sys.exit(0)
