#!/usr/bin/env python

import geni.portal as portal
from geni.namespaces import Namespace
import geni.rspec.pg as RSpec
import geni.rspec.igext as IG
# Emulab specific extensions.
import geni.rspec.emulab as emulab
from lxml import etree as ET
import crypt
import random
import os.path
import sys

extns = "http://www.protogeni.net/resources/rspec/ext/johnsond/osa/1"

class Label(RSpec.Resource):
    def __init__(self,name,value):
        self.name = name
        self.value = value

    def _write(self,root):
        el = ET.SubElement(root,"{%s}label" % (extns,))
        el.attrib["name"] = self.name
        el.text = self.value
        return el
RSpec.Node.EXTENSIONS.append(("Label",Label))
RSpec.Link.EXTENSIONS.append(("Label",Label))

TBCMD = "sudo mkdir -p /root/setup && sudo -H /local/repository/setup-driver.sh 2>&1 | sudo tee /root/setup/setup-driver.log"

#
# For now, disable the testbed's root ssh key service until we can remove ours.
# It seems to race (rarely) with our startup scripts.
#
disableTestbedRootKeys = True

#
# Create our in-memory model of the RSpec -- the resources we're going to request
# in our experiment, and their configuration.
#
rspec = RSpec.Request()
rspec.addNamespace(Namespace("osa",extns))

#
# This geni-lib script is designed to run in the CloudLab Portal.
#
pc = portal.Context()

#
# Define *many* parameters; see the help docs in geni-lib to learn how to modify.
#
pc.defineParameter(
    "release","OpenStack Release",portal.ParameterType.STRING,"rocky",
    [("rocky","Rocky"),("queens","Queens"),("pike","Pike"),
     ("ocata","Ocata"),("newton","Newton"),("mitaka","Mitaka")],
    longDescription="OpenStack-Ansible provides Rocky, Queens (Ubuntu 18.04), Pike, Ocata, Newton, Mitaka (Ubuntu 16.04).  OpenStack is installed from source, or from packages available on these distributions.")
pc.defineParameter(
    "computeNodeCount", "Number of compute nodes",
    portal.ParameterType.INTEGER, 1)
pc.defineParameter(
    "osNodeType", "Hardware Type",
    portal.ParameterType.NODETYPE, "",
    longDescription="A specific hardware type to use for each node.  Cloudlab clusters all have machines of specific types.  When you set this field to a value that is a specific hardware type, you will only be able to instantiate this profile on clusters with machines of that type.  If unset, when you instantiate the profile, the resulting experiment may have machines of any available type allocated.")
pc.defineParameter(
    "osLinkSpeed", "Experiment Link Speed",
    portal.ParameterType.INTEGER, 0,
    [(0,"Any"),(1000000,"1Gb/s"),(10000000,"10Gb/s"),(25000000,"25Gb/s"),
     (40000000,"40Gb/s")],
    longDescription="A specific link speed to use for each node.  All experiment network interfaces will request this speed.")
pc.defineParameter(
    "ml2plugin","ML2 Plugin",
    portal.ParameterType.STRING,"linuxbridge",
    [("linuxbridge","Linux Bridge"),("openvswitch","OpenVSwitch")],
    longDescription="OpenStack-Ansible supports both the OpenVSwitch and LinuxBridge ML2 plugins to create virtual networks in Neutron.  LinuxBridge is the default and best-supported option.")
pc.defineParameter(
    "extraImageURLs","Extra VM Image URLs",
    portal.ParameterType.STRING,"",
    longDescription="This parameter allows you to specify a space-separated list of URLs, each of which points to an OpenStack VM image, which we will download and slighty tweak before uploading to Glance in your OpenStack experiment.")
pc.defineParameter(
    "firewall","Experiment Firewall",
    portal.ParameterType.BOOLEAN,False,
    longDescription="Optionally add a CloudLab infrastructure firewall between the public IP addresses of your nodes (and your floating IPs) and the Internet (and rest of CloudLab).")

pc.defineParameter(
    "clusterCount","Number of OpenStack clusters",
    portal.ParameterType.INTEGER,1,advanced=True,
    longDescription="You can create multiple OpenStack clusters within a single experiment by setting this parameter > 1.  Note that any settings you have chosen will be applied to each cluster, other than those regarding remote blockstore mounting.")
pc.defineParameter(
    "clusterChooseSite","Choose Site for each OpenStack cluster",
    portal.ParameterType.BOOLEAN,False,advanced=True,
    longDescription="If you want to choose which CloudLab site each OpenStack cluster will be instantiated at, select this parameter.")
pc.defineParameter(
    "connectControllers","Connect Controllers in LAN",
    portal.ParameterType.BOOLEAN,True,advanced=True,
    longDescription="Connect controller nodes to a LAN, if more than one cluster.")

pc.defineParameter(
    "ubuntuMirrorHost","Ubuntu Package Mirror Hostname",
    portal.ParameterType.STRING,"",advanced=True,
    longDescription="A specific Ubuntu package mirror host to use instead of us.archive.ubuntu.com (mirror must have Ubuntu in top-level dir, or you must also edit the mirror path parameter below)")
pc.defineParameter(
    "ubuntuMirrorPath","Ubuntu Package Mirror Path",
    portal.ParameterType.STRING,"",advanced=True,
    longDescription="A specific Ubuntu package mirror path to use instead of /ubuntu/ (you must also set a value for the package mirror parameter)")
pc.defineParameter(
    "doAptDistUpgrade","Upgrade all packages to their latest versions",
    portal.ParameterType.BOOLEAN, False,advanced=True,
    longDescription="If you select this option, we will run an apt-get dist-upgrade to upgrade all packages installed on the node.")

pc.defineParameter(
    "publicIPCount", "Number of public IP addresses",
    portal.ParameterType.INTEGER, 4,advanced=True,
    longDescription="Make sure to include both the number of floating IP addresses you plan to need for instances; and also for OpenVSwitch interface IP addresses.  Each OpenStack network this profile creates for you is bridged to the external, public network, so you also need a public IP address for each of those switch interfaces.  So, if you ask for one tunnel network, and one flat data network (the default configuration), you would need two public IPs for switch interfaces, and then you request two additional public IPs that can be bound to instances as floating IPs.  If you ask for more networks, make sure to increase this number appropriately.")
pc.defineParameter(
    "flatDataLanCount","Number of Flat Data Networks",
    portal.ParameterType.INTEGER,1,advanced=True,
    longDescription="Create a number of flat OpenStack networks.  If you do not select the Multiplex Flat Networks option below, each of these networks requires a physical network interface.  If you attempt to instantiate this profile on nodes with only 1 experiment interface, and ask for more than one flat network, your profile will not instantiate correctly.  Many CloudLab nodes have only a single experiment interface.")
pc.defineParameter(
    "vxlanDataLanCount","Number of VXLAN Data Networks",
    portal.ParameterType.INTEGER,0,
    longDescription="To use VXLAN networks, you must have at least one flat data network; all tunnels are implemented using the first flat network!",
    advanced=True)
pc.defineParameter(
    "vlanDataLanCount","Number of VLAN Data Networks",
    portal.ParameterType.INTEGER,0,advanced=True,
    longDescription="If you want to play with OpenStack networks that are implemented using real VLAN tags, create VLAN-backed networks with this parameter.  Currently, however, you cannot combine it with Flat nor Tunnel data networks.")

pc.defineParameter(
    "multiplexFlatLans", "Multiplex Flat Networks",
    portal.ParameterType.BOOLEAN, False,
    longDescription="Multiplex any flat networks (i.e., management and all of the flat data networks) over physical interfaces, using VLANs.  These VLANs are invisible to OpenStack, unlike the NUmber of VLAN Data Networks option, where OpenStack assigns the real VLAN tags to create its networks.  On CloudLab, many physical machines have only a single experiment network interface, so if you want multiple flat networks, you have to multiplex.  Currently, if you select this option, you *must* specify 0 for VLAN Data Networks; we cannot support both simultaneously yet.",
    advanced=True)

pc.defineParameter(
    "swiftLVSize", "Swift Logical Volume Size",
    portal.ParameterType.INTEGER,4,advanced=True,
    longDescription="The necessary space in GB to reserve for each of two Swift backing store volumes, when it is possible to use logical volumes.  Nearly all Cloudlab machines do support logical volumes.  Ensure that the total disk space requested (20GB root + 2x Swift LV size + 1x Glance LV size) is less than the total disk space available on the node type you want to run on.")
pc.defineParameter(
    "glanceLVSize", "Glance Logical Volume Size",
    portal.ParameterType.INTEGER,32,advanced=True,
    longDescription="The necessary space in GB to reserve for a Glance backing store for disk images, when it is possible to use logical volumes.  Nearly all Cloudlab machines do support logical volumes.  Ensure that the total disk space requested (20GB root + 2x Swift LV size + 1x Glance LV size) is less than the total disk space available on the node type you want to run on.")
pc.defineParameter(
    "tempBlockstoreMountPoint", "Temporary Filesystem Mount Point",
    portal.ParameterType.STRING,"",advanced=True,
    longDescription="Mounts an ephemeral, temporary filesystem at this mount point, on the nodes which you specify below.  If you specify no nodes, and specify a mount point here, all nodes will get a temp filesystem.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken).")
pc.defineParameter(
    "tempBlockstoreSize", "Temporary Filesystem Size",
    portal.ParameterType.INTEGER, 0,advanced=True,
    longDescription="The necessary space in GB to reserve for your temporary filesystem.")
pc.defineParameter(
    "tempBlockstoreMountNodes", "Temporary Filesystem Mount Node(s)",
    portal.ParameterType.STRING,"",advanced=True,
    longDescription="The node(s) on which you want a temporary filesystem created; space-separated for more than one.  Leave blank if you want all nodes to have a temp filesystem.")

pc.defineParameter(
    "blockstoreURN", "Remote Dataset URN",
    portal.ParameterType.STRING, "",advanced=True,
    longDescription="The URN of an *existing* remote dataset (a remote block store) that you want attached to the node you specified (defaults to the ctl node).  The block store must exist at the cluster at which you instantiate the profile!")
pc.defineParameter(
    "blockstoreMountNode", "Remote Dataset Mount Node",
    portal.ParameterType.STRING, "ctl",advanced=True,
    longDescription="The node on which you want your remote block store mounted; defaults to the controller node.")
pc.defineParameter(
    "blockstoreMountPoint", "Remote Dataset Mount Point",
    portal.ParameterType.STRING, "/dataset",advanced=True,
    longDescription="The mount point at which you want your remote dataset mounted.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken).  Note also that this option requires a network interface, because it creates a link between the dataset and the node where the dataset is available.  Thus, just as for creating extra LANs, you might need to select the Multiplex Flat Networks option, which will also multiplex the blockstore link here.")
pc.defineParameter(
    "blockstoreReadOnly", "Mount Remote Dataset Read-only",
    portal.ParameterType.BOOLEAN, True,advanced=True,
    longDescription="Mount the remote dataset in read-only mode.")

pc.defineParameter(
    "localBlockstoreURN", "Image-backed Dataset URN",
    portal.ParameterType.STRING, "",advanced=True,
    longDescription="The URN of an image-backed dataset that already exists that you want loaded into the node you specified (defaults to the ctl node).  The block store must exist at the cluster at which you instantiate the profile!")
pc.defineParameter(
    "localBlockstoreMountNode", "Image-backed Dataset Mount Node",
    portal.ParameterType.STRING, "ctl",advanced=True,
    longDescription="The node on which you want your image-backed dataset mounted; defaults to the controller node.")
pc.defineParameter(
    "localBlockstoreMountPoint", "Image-Backed Dataset Mount Point",
    portal.ParameterType.STRING, "/image-dataset",advanced=True,
    longDescription="The mount point at which you want your image-backed dataset mounted.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken).")
pc.defineParameter(
    "localBlockstoreSize", "Image-Backed Dataset Size",
    portal.ParameterType.INTEGER, 0,advanced=True,
    longDescription="The necessary space to reserve for your image-backed dataset (you should set this to at least the minimum amount of space your image-backed dataset will require).")
pc.defineParameter(
    "localBlockstoreReadOnly", "Mount Image-Backed Dataset Read-only",
    portal.ParameterType.BOOLEAN, True,advanced=True,
    longDescription="Mount the image-backed dataset in read-only mode.")

pc.defineParameter(
    "quotasOff","Unlimit Default Quotas",
    portal.ParameterType.BOOLEAN,True,advanced=True,
    longDescription="Set the default Nova and Cinder quotas to unlimited, at least those that can be set via CLI utils (some cannot be set, but the significant ones can be set).")

pc.defineParameter(
    "disableSecurityGroups","Disable Security Group Enforcement",
    portal.ParameterType.BOOLEAN,False,advanced=True,
    longDescription="Sometimes it can be easier to play with OpenStack if you do not have to mess around with security groups at all.  This option selects a null security group driver, if set.  This means security groups are enabled, but are not enforced (we set the firewall_driver neutron option to neutron.agent.firewall.NoopFirewallDriver to accomplish this).")

pc.defineParameter(
    "enableHostPassthrough","Enable Host Passthrough",
    portal.ParameterType.BOOLEAN,True,advanced=True,
    longDescription="Signals KVM to pass through the host CPU with no modifications. The difference to host-model, instead of just matching feature flags, every last detail of the host CPU is matched. This gives the best performance but comes at a cost with respect to migration. The guest can only be migrated to a matching host CPU.")

pc.defineParameter(
    "enableInboundSshAndIcmp","Enable Inbound SSH and ICMP",
    portal.ParameterType.BOOLEAN,True,advanced=True,
    longDescription="Enable inbound SSH and ICMP into your instances in the default security group, if you have security groups enabled.")

pc.defineParameter(
    "enableVerboseLogging","Enable Verbose Logging",
    portal.ParameterType.BOOLEAN,False,advanced=True,
    longDescription="Enable verbose logging for OpenStack components.")
pc.defineParameter(
    "enableDebugLogging","Enable Debug Logging",
    portal.ParameterType.BOOLEAN,False,advanced=True,
    longDescription="Enable debug logging for OpenStack components.")

pc.defineParameter(
    "controllerBaseName", "Base name of controller node(s)",
    portal.ParameterType.STRING, "ctl", advanced=True,
    longDescription="The short name of the controller node.  You shold leave this alone unless you really want the hostname to change.")
pc.defineParameter(
    "computeBaseName", "Base name of compute node(s)",
    portal.ParameterType.STRING, "cp", advanced=True,
    longDescription="The base string of the short name of the compute nodes (node names will look like cp-1, cp-2, ... ).  You should leave this alone unless you really want the hostname to change.")
pc.defineParameter(
    "firewallStyle","Firewall Style",
    portal.ParameterType.STRING,"none",
    [("none","None"),("basic","Basic"),("closed","Closed")],
    advanced=True,
    longDescription="Optionally add a CloudLab infrastructure firewall between the public IP addresses of your nodes (and your floating IPs) and the Internet (and rest of CloudLab).  The choice you make for this parameter controls the firewall ruleset, if not None.  None means no firewall; Basic implies a simple firewall that allows inbound SSH and outbound HTTP/HTTPS traffic; Closed implies a firewall ruleset that allows *no* communication with the outside world or other experiments within CloudLab.  If you are unsure, the Basic style is the one that will work best for you.")
pc.defineParameter(
    "controllerDiskImage","Controller Node Disk Image",
    portal.ParameterType.IMAGE,"",advanced=True,
    longDescription="An image URN or URL that the controller node will run.")
pc.defineParameter(
    "computeDiskImage","Compute Node Disk Image",
    portal.ParameterType.IMAGE,"",advanced=True,
    longDescription="An image URN or URL that the compute node will run.")
#pc.defineParameter("blockStorageHost", "Name of block storage server node",
#                   portal.ParameterType.STRING, "ctl")
#pc.defineParameter("objectStorageHost", "Name of object storage server node",
#                   portal.ParameterType.STRING, "ctl")
#pc.defineParameter("blockStorageNodeCount", "Number of block storage nodes",
#                   portal.ParameterType.INTEGER, 0)
#pc.defineParameter("objectStorageNodeCount", "Number of object storage nodes",
#                   portal.ParameterType.STRING, 0)

#
# Get any input parameter values that will override our defaults.
#
params = pc.bindParameters()

#
# Verify our parameters and throw errors.
#
# Just set the firewall style to something sane if they want a firewall.
if params.firewall == True and params.firewallStyle == 'none':
    params.firewallStyle = 'basic'

if params.release in [ 'juno','kilo','liberty' ] \
  and (not params.firewall or params.firewallStyle == 'none'):
    perr = portal.ParameterError("To use deprecated OpenStack releases, you *must* place your nodes behind an infrastructure firewall, by enabling the Firewall parameter.  These releases rely on insecure, out-of-date software.",['release','firewall'])
    pc.reportError(perr)
if params.computeNodeCount > 8:
    perr = portal.ParameterWarning("Are you creating a real cloud?  Otherwise, do you really need more than 8 compute nodes?  Think of your fellow users scrambling to get nodes :).",['computeNodeCount'])
    pc.reportWarning(perr)
if params.publicIPCount > 16:
    perr = portal.ParameterWarning("You cannot request more than 16 public IP addresses, at least not without creating your own modified version of this profile!",['publicIPCount'])
    pc.reportWarning(perr)
if (params.vlanDataLanCount + params.vxlanDataLanCount \
    + params.flatDataLanCount) \
    > (params.publicIPCount - 1):
    perr = portal.ParameterWarning("You did not request enough public IPs to cover all your data networks and still leave you at least one floating IP; you may want to read this parameter's help documentation and change your parameters!",['publicIPCount'])
    pc.reportWarning(perr)
    pass

if params.vlanDataLanCount > 0 and params.flatDataLanCount > 0:
    perr = portal.ParameterError("You cannot specify vlanDataLanCount > 0 and flatDataLanCount > 0",['vlanDataLanCount','flatDataLanCount'])
    pc.reportError(perr)
    pass
if params.vxlanDataLanCount > 0 and params.flatDataLanCount < 1:
    perr = portal.ParameterError("You must specifiy at least one flat data network to request one or more VXLAN data networks!",['vxlanDataLanCount','flatDataLanCount'])
    pc.reportError(perr)
    pass

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

detailedParamAutoDocs = ''
for param in pc._parameterOrder:
    if not pc._parameters.has_key(param):
        continue
    detailedParamAutoDocs += \
      """
  - *%s*

    %s
    (default value: *%s*)
      """ % (pc._parameters[param]['description'],pc._parameters[param]['longDescription'],pc._parameters[param]['defaultValue'])
    pass

tourDescription = \
  "This profile provides one or more configurable OpenStack-Ansible instances, each with a controller and one or more compute nodes. This profile runs x86, arm64, and POWER8 (Queens and up) nodes. It sets up OpenStack Rocky, Queens (Ubuntu 18.04), Pike, Ocata, Newton, or Mitaka (Ubuntu 16.04) according to your choice, and configures all OpenStack services, pulls in some VM disk images, and creates basic networks accessible via floating IPs.  You'll be able to create instances and access them over the Internet in just a few minutes. When you click the Instantiate button, you'll be presented with a list of parameters that you can change to control what your OpenStack instance will look like; **carefully** read the parameter documentation on that page (or in the Instructions) to understand the various features available to you."

###if not params.adminPass or len(params.adminPass) == 0:
passwdHelp = "Your OpenStack admin and instance VM password is randomly-generated by Cloudlab, and it is: `{password-adminPass}` ."
###else:
###    passwdHelp = "Your OpenStack dashboard and instance VM password is `the one you specified in parameter selection`; hopefully you memorized or memoized it!"
###    pass
passwdHelp += "  When logging in to the Dashboard, use the `admin` user; when logging into instance VMs, use the `ubuntu` user.  If you have selected Mitaka or newer, use 'default' as the Domain at the login prompt."

grafanaInstructions = ""
if params.release in [ "pike","queens","rocky" ]:
    grafanaInstructions = "You can also login to [your experiment's Grafana WWW interface](http://{host-%s}:3000/dashboard/db/openstack-instance-statistics?orgId=1) and view OpenStack statistics once you've created some VMs." % (params.controllerBaseName)

tourInstructions = \
  """
### Basic Instructions
Once your experiment nodes have booted, and this profile's configuration scripts have finished configuring OpenStack inside your experiment, you'll be able to visit [the OpenStack Dashboard WWW interface](http://{host-%s}/horizon/auth/login/?next=/horizon/project/instances/) (approx. 5-15 minutes).  %s  %s

Please wait to login to the OpenStack dashboard until the setup scripts have completed (we've seen Dashboard issues with content not appearing if you login before configuration is complete).  There are multiple ways to determine if the scripts have finished:
  - First, you can watch the experiment status page: the overall State will say \"booted (startup services are still running)\" to indicate that the nodes have booted up, but the setup scripts are still running.
  - Second, the Topology View will show you, for each node, the status of the startup command on each node (the startup command kicks off the setup scripts on each node).  Once the startup command has finished on each node, the overall State field will change to \"ready\".  If any of the startup scripts fail, you can mouse over the failed node in the topology viewer for the status code.
  - Third, the profile configuration scripts also send you two emails: once to notify you that controller setup has started, and a second to notify you that setup has completed.  Once you receive the second email, you can login to the Openstack Dashboard and begin your work.
  - Finally, you can view [the profile setup script logfiles](http://{host-%s}:7999/) as the setup scripts run.  Use the `admin` username and the random password above.

**NOTE:** If the web interface rejects your password or gives another error, the scripts might simply need more time to set up the backend. Wait a few minutes and try again.  If you don't receive any email notifications, you can SSH to the 'ctl' node, become root, and check the primary setup script's logfile (/root/setup/setup-controller.log).  If near the bottom there's a line that includes 'Your OpenStack instance has completed setup'), the scripts have finished, and it's safe to login to the Dashboard.

If you need to run the OpenStack CLI tools, or your own scripts that use the OpenStack APIs, you'll find authentication credentials in /root/setup/admin-openrc.sh .  Be aware that the username in this file is `adminapi`, not `admin`; this is an artifact of the days when the profile used to allow you to customize the admin password (it was necessary because the nodes did not have the plaintext password, but only the hash).

*Do not* add any VMs on the `ext-net` network; instead, give them floating IP addresses from the pool this profile requests on your behalf (and increase the size of that pool when you instantiate by changing the `Number of public IP addresses` parameter).  If you try to use any public IP addresses on the `ext-net` network that are not part of your experiment (i.e., any that are not either the control network public IPs for the physical machines, or the public IPs used as floating IPs), those packets will be blocked, and you will be confused.

The profile's setup scripts are automatically installed on each node in `/tmp/setup` .  They execute as `root`, and keep state and downloaded files in `/root/setup/`.  More importantly, they write copious logfiles in that directory; so if you think there's a problem with the configuration, you could take a quick look through these logs --- especially `setup-controller.log` on the `ctl` node.


### Detailed Parameter Documentation
%s
""" % (params.controllerBaseName,grafanaInstructions,passwdHelp,params.controllerBaseName,detailedParamAutoDocs)

#
# Setup the Tour info with the above description and instructions.
#  
tour = IG.Tour()
tour.Description(IG.Tour.TEXT,tourDescription)
tour.Instructions(IG.Tour.MARKDOWN,tourInstructions)
rspec.addTour(tour)

#
# We create CloudLab LANs to be used as openstack networks, based on
# user's parameters.  We might also generate IP addresses for the nodes,
# so set up a simple IP address generation for each LAN.
#
mlanstrs = {}
flatlanstrs = {}
vlanstrs = {}
ipdb = {}

for cc in range(1,params.clusterCount + 1):
    flatlanstrs[cc] = {}
    vlanstrs[cc] = {}
    mlanstrs[cc] = None

    # Reserve a subnet for the management network.
    mlanstr = 'mgmt-lan-c%d' % (cc,)
    ipdb[mlanstr] = {
        'base':'192.168','netmask':'255.255.0.0','values':[-1,-1,0,0]
    }
    mlanstrs[cc] = mlanstr

    #
    # Note that some things below the dataOffset of 10, we use for other
    # things; for instance, shared vlan addresses should be allocated in the
    # 10.10/16 or 10.10.10/24 subnets.
    #
    dataOffset = 10
    ipSubnetsUsed = 0
    for i in range(1,params.flatDataLanCount + 1):
        dlanstr = "%s-%d-c%d" % ('flat-lan',i,cc)
        ipdb[dlanstr] = {
            'base' : '10.%d' % (i + dataOffset + ipSubnetsUsed,),
            'netmask' : '255.255.0.0','values' : [-1,-1,10,0] }
        flatlanstrs[cc][i] = dlanstr
        ipSubnetsUsed += 1
        pass
    for i in range(1,params.vlanDataLanCount + 1):
        dlanstr = "%s-%d" % ('vlan-lan',i)
        ipdb[dlanstr] = {
            'base' : '10.%d' % (i + dataOffset + ipSubnetsUsed,),
            'netmask' : '255.255.0.0','values' : [-1,-1,10,0] }
        vlanstrs[cc][i] = dlanstr
        ipSubnetsUsed += 1
        pass
    for i in range(1,params.vxlanDataLanCount + 1):
        dlanstr = "%s-%d" % ('vxlan-lan',i)
        ipdb[dlanstr] = {
            'base' : '10.%d' % (i + dataOffset + ipSubnetsUsed,),
            'netmask' : '255.255.0.0','values' : [-1,-1,10,0] }
        ipSubnetsUsed += 1
        pass

# Assume a /16 for every network
def get_next_ipaddr(lan):
    ipaddr = ipdb[lan]['base']
    backpart = ''

    idxlist = range(1,4)
    idxlist.reverse()
    didinc = False
    for i in idxlist:
        if ipdb[lan]['values'][i] is -1:
            break
        if not didinc:
            didinc = True
            ipdb[lan]['values'][i] += 1
            if ipdb[lan]['values'][i] > 254:
                if ipdb[lan]['values'][i-1] is -1:
                    return ''
                else:
                    ipdb[lan]['values'][i-1] += 1
                    pass
                pass
            pass
        backpart = '.' + str(ipdb[lan]['values'][i]) + backpart
        pass

    return ipaddr + backpart

def get_netmask(lan):
    return ipdb[lan]['netmask']

#
# If we are building multiple OpenStack clusters, maybe add a LAN
# connecting them.
#
interconnect = None
if params.clusterCount > 1 and params.connectControllers:
    lanstr = "interconnect"
    ipdb[lanstr] = {
        'base' : '10.222','netmask' : '255.255.0.0','values' : [-1,-1,10,0] }
    ipSubnetsUsed += 1
    interconnect = RSpec.LAN(lanstr)
    interconnect._ext_children.append(Label("role","interconnect"))
    if params.multiplexFlatLans:
        interconnect.link_multiplexing = True
        interconnect.best_effort = True
        interconnect.type = "vlan"

#
# Build the data/mgmt LANs now.
#
mlans = {}
flatlans = {}
vlans = {}
alllans = {}

for cc in range(1,params.clusterCount + 1):
    mlans[cc] = None
    flatlans[cc] = {}
    vlans[cc] = {}
    alllans[cc] = []

    for i in range(1,params.flatDataLanCount + 1):
        datalan = RSpec.LAN(flatlanstrs[cc][i])
        datalan._ext_children.append(Label("cluster","c%d" % (cc,)))
        datalan._ext_children.append(Label("role","flatlan"))
        if params.osLinkSpeed > 0:
            datalan.bandwidth = int(params.osLinkSpeed)
        if params.multiplexFlatLans:
            datalan.link_multiplexing = True
            datalan.best_effort = True
            datalan.type = "vlan"
        flatlans[cc][i] = datalan
        alllans[cc].append(datalan)
    for i in range(1,params.vlanDataLanCount + 1):
        datalan = RSpec.LAN("vlan-lan-%d" % (i,))
        datalan._ext_children.append(Label("cluster","c%d" % (cc,)))
        datalan._ext_children.append(Label("role","vlan"))
        if params.osLinkSpeed > 0:
            datalan.bandwidth = int(params.osLinkSpeed)
        datalan.link_multiplexing = True
        datalan.best_effort = True
        datalan.type = "vlan"
        vlans[cc][i] = datalan
        alllans[cc].append(datalan)

    mgmtlan = RSpec.LAN(mlanstrs[cc])
    mgmtlan._ext_children.append(Label("cluster","c%d" % (cc,)))
    mgmtlan._ext_children.append(Label("role","mgmtlan"))
    if params.multiplexFlatLans:
        mgmtlan.link_multiplexing = True
        mgmtlan.best_effort = True
        mgmtlan.type = "vlan"
    mlans[cc] = mgmtlan

#
# Construct the disk image URNs we're going to set the various nodes to load.
# NB: we stopped generating OSNM images at Rocky for x86/aarch64; and at
# Queens for ppc64le.
#
image_project = 'emulab-ops'
image_urn = 'emulab.net'
if params.release in [ 'mitaka','newton','ocata','pike' ]:
    image = 'UBUNTU16-64-STD'
else:
    image = 'UBUNTU18-64-STD'

#
# XXX: special handling for ppc64le at Clemson because of special disk
# image names for UBUNTU18-64-STD and UBUNTU18-*OSC*-Q, and because only
# >= Queens is available for them.
#
if params.osNodeType == 'ibm8335':
    image = 'UBUNTU18-PPC64LE'
    if params.release not in [ 'queens','rocky' ]:
        perr = portal.ParameterError(
            "You can only run the Queens release, or greater, on `ibm8335` (POWER8) hardware!",
            ['release','osNodeType'])
        pc.reportError(perr)
        pc.verifyParameters()

    if params.ml2plugin != 'linuxbridge':
        perr = portal.ParameterWarning(
            "The openvswitch plugin may not work correct on POWER8; you might consider changing to the linuxbridge plugin, which works fine.",
            ['ml2plugin','vxlanDataLanCount'])
        pc.reportWarning(perr)
        pc.verifyParameters()

nodes = {}
controllers = []
apools = []

fwrules = [
    # Protogeni xmlrpc
    "iptables -A INSIDE -p tcp --dport 12369 -j ACCEPT",
    "iptables -A INSIDE -p tcp --dport 12370 -j ACCEPT",
    # Inbound http to the controller node.
    "iptables -A OUTSIDE -p tcp -d ctl.EMULAB_EXPDOMAIN --dport 80 -j ACCEPT",
    # Inbound VNC to any host (only need for compute hosts, but hard to
    # specify that).
    "iptables -A OUTSIDE -p tcp --dport 6080 -j ACCEPT",
]

# Firewall node, Site 1.
firewalling = False
setfwdesire = True
if params.firewallStyle in ('open','closed','basic'):
    firewalling = True

#
# Handle temp blockstore param.  Note that we do not generate errors for
# non-existent nodes!
#
tempBSNodes = []
if params.tempBlockstoreMountPoint != "":
    if params.tempBlockstoreMountNodes:
        tempBSNodes = params.tempBlockstoreMountNodes.split()
    if params.tempBlockstoreSize <= 0:
        perr = portal.ParameterError("Your temporary filesystems must have size > 0!",
                                     ['tempBlockstoreSize'])
        pc.reportError(perr)
        pc.verifyParameters()
    pass

if firewalling:
    fw = rspec.ExperimentFirewall('fw',params.firewallStyle)
    fw.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD'
    fw.Site("1")
    if params.osNodeType:
        fw.hardware_type = params.osNodeType
    for rule in fwrules:
        fw.addRule(rule)

for cc in range(1,params.clusterCount + 1):
    #
    # Add the controller node.
    #
    name = params.controllerBaseName + "-c%d" % (cc,)
    controller = RSpec.RawPC(name)
    controllers.append(controller)
    controller._ext_children.append(Label("role","controller"))
    controller._ext_children.append(Label("cluster","c%d" % (cc,)))
    nodes[name] = controller
    if params.osNodeType:
        controller.hardware_type = params.osNodeType
    if params.clusterChooseSite:
        controller.Site(str(cc))
    else:
        controller.Site("1")
    if params.controllerDiskImage:
        controller.disk_image = params.controllerDiskImage
    else:
        controller.disk_image = "urn:publicid:IDN+%s+image+%s//%s" % (image_urn,image_project,image)
    if firewalling and setfwdesire:
        controller.Desire('firewallable','1.0')
    i = 0
    if interconnect:
        iface = controller.addInterface("ifInter")
        interconnect.addInterface(iface)
        iface.addAddress(RSpec.IPv4Address(get_next_ipaddr(interconnect.client_id),
                                           get_netmask(interconnect.client_id)))
    for datalan in alllans[cc]:
        iface = controller.addInterface("if%d" % (i,))
        datalan.addInterface(iface)
        iface.addAddress(RSpec.IPv4Address(get_next_ipaddr(datalan.client_id),
                                           get_netmask(datalan.client_id)))
        i += 1
    iface = controller.addInterface("ifM")
    mgmtlan = mlans[cc]
    mgmtlan.addInterface(iface)
    iface.addAddress(RSpec.IPv4Address(get_next_ipaddr(mgmtlan.client_id),
                                       get_netmask(mgmtlan.client_id)))
    controller.addService(RSpec.Execute(shell="sh",command=TBCMD))
    if disableTestbedRootKeys:
        controller.installRootKeys(False, False)
    if params.tempBlockstoreMountPoint \
        and (len(tempBSNodes) == 0 or name in tempBSNodes):
        bs = controller.Blockstore(
            name + "-temp-bs",params.tempBlockstoreMountPoint)
        bs.size = str(params.tempBlockstoreSize) + "GB"
        bs.placement = "any"

    #
    # Grab a few public IP addresses.
    #
    apool = IG.AddressPool(name,params.publicIPCount)
    try:
        apool.Site(str(cc))
    except:
        pass
    apools.append(apool)

    #
    # Add the compute nodes.
    #
    for i in range(1,params.computeNodeCount + 1):
        cpname = "%s-%d-c%d" % (params.computeBaseName,i,cc)
        cpnode = RSpec.RawPC(cpname)
        cpnode._ext_children.append(Label("role","compute"))
        cpnode._ext_children.append(Label("cluster","c%d" % (cc,)))

        nodes[cpname] = cpnode
        if params.osNodeType:
            cpnode.hardware_type = params.osNodeType
        if params.clusterChooseSite:
            cpnode.Site(str(cc))
        else:
            cpnode.Site("1")
        if params.computeDiskImage:
            cpnode.disk_image = params.computeDiskImage
        else:
            cpnode.disk_image = "urn:publicid:IDN+%s+image+%s//%s" % (image_urn,image_project,image)
        if firewalling and setfwdesire:
            cpnode.Desire('firewallable','1.0')
        i = 0
        for datalan in alllans[cc]:
            iface = cpnode.addInterface("if%d" % (i,))
            datalan.addInterface(iface)
            iface.addAddress(RSpec.IPv4Address(get_next_ipaddr(datalan.client_id),
                                               get_netmask(datalan.client_id)))
            i += 1
        iface = cpnode.addInterface("ifM")
        mgmtlan = mlans[cc]
        mgmtlan.addInterface(iface)
        iface.addAddress(RSpec.IPv4Address(get_next_ipaddr(mgmtlan.client_id),
                                            get_netmask(mgmtlan.client_id)))
        cpnode.addService(RSpec.Execute(shell="sh",command=TBCMD))
        if disableTestbedRootKeys:
            cpnode.installRootKeys(False, False)
        if params.tempBlockstoreMountPoint \
          and (len(tempBSNodes) == 0 or cpname in tempBSNodes):
            bs = cpnode.Blockstore(
                cpname+"-temp-bs",params.tempBlockstoreMountPoint)
            bs.size = str(params.tempBlockstoreSize) + "GB"
            bs.placement = "any"
        pass
    pass

#
# Add the blockstore, if requested.
#
bsnode = None
bslink = None
if params.blockstoreURN != "":
    if not nodes.has_key(params.blockstoreMountNode):
        #
        # This is a very late time to generate a warning, but that's ok!
        #
        perr = portal.ParameterError("The node on which you mount your remote dataset must exist, and does not!",
                                     ['blockstoreMountNode'])
        pc.reportError(perr)
        pc.verifyParameters()
        pass
    
    rbsn = nodes[params.blockstoreMountNode]
    myintf = rbsn.addInterface("ifbs0")
    
    bsnode = IG.RemoteBlockstore("bsnode",params.blockstoreMountPoint)
    bsnode.Site("1")
    if firewalling and setfwdesire:
        bsnode.Desire('firewallable','1.0')
    bsintf = bsnode.interface
    bsnode.dataset = params.blockstoreURN
    #bsnode.size = params.N
    bsnode.readonly = params.blockstoreReadOnly
    
    bslink = RSpec.Link("bslink")
    bslink.addInterface(myintf)
    bslink.addInterface(bsintf)
    # Special blockstore attributes for this link.
    bslink.best_effort = True
    bslink.vlan_tagging = True
    pass

#
# Add the local blockstore, if requested.
#
lbsnode = None
if params.localBlockstoreURN != "":
    if not nodes.has_key(params.localBlockstoreMountNode):
        #
        # This is a very late time to generate a warning, but that's ok!
        #
        perr = portal.ParameterError("The node on which you mount your image-backed dataset must exist, and does not!",
                                     ['localBlockstoreMountNode'])
        pc.reportError(perr)
        pc.verifyParameters()
        pass
    if params.localBlockstoreSize is None or params.localBlockstoreSize <= 0 \
      or str(params.localBlockstoreSize) == "":
        #
        # This is a very late time to generate a warning, but that's ok!
        #
        perr = portal.ParameterError("You must specify a size (> 0) for your image-backed dataset!",
                                     ['localBlockstoreSize'])
        pc.reportError(perr)
        pc.verifyParameters()
        pass

    lbsn = nodes[params.localBlockstoreMountNode]
    lbsnode = lbsn.Blockstore("lbsnode",params.localBlockstoreMountPoint)
    lbsnode.dataset = params.localBlockstoreURN
    lbsnode.size = str(params.localBlockstoreSize)
    lbsnode.readonly = params.localBlockstoreReadOnly
    pass

for nname in nodes.keys():
    rspec.addResource(nodes[nname])
if bsnode:
    rspec.addResource(bsnode)
for cc in range(1,params.clusterCount + 1):
    for datalan in alllans[cc]:
        rspec.addResource(datalan)
    rspec.addResource(mlans[cc])
if interconnect:
    rspec.addResource(interconnect)
if bslink:
    rspec.addResource(bslink)
    pass
for apool in apools:
    rspec.addResource(apool)

class EmulabEncrypt(RSpec.Resource):
    def _write(self, root):
        ns = "{http://www.protogeni.net/resources/rspec/ext/emulab/1}"

#        el = ET.SubElement(root,"%sencrypt" % (ns,),attrib={'name':'adminPass'})
#        el.text = params.adminPass
        el = ET.SubElement(root,"%spassword" % (ns,),attrib={'name':'adminPass'})
        pass
    pass

stuffToEncrypt = EmulabEncrypt()
rspec.addResource(stuffToEncrypt)

pc.printRequestRSpec(rspec)
