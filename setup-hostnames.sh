#!/bin/sh

set -x

if [ -z "$EUID" ]; then
    EUID=`id -u`
fi
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "`dirname $0`/setup-lib.sh"

if [ -f $OURDIR/hostnames-done ]; then
    exit 0
fi

logtstart "hostnames"

#
# Get our hosts files setup to point to the new management network.
# (These were created one-time in setup-lib.sh)
#
cat $OURDIR/mgmt-hosts > /etc/hosts.tmp
# Some services assume they can resolve the hostname prior to network being
# up (i.e. neutron-ovs-cleanup; see setup-ovs-node.sh).
echo $MYIP `hostname` >> /etc/hosts.tmp
cp -p /etc/hosts $OURDIR/hosts.orig
cp -p /etc/hosts $OURDIR/hosts.stripped
for node in $ALLNODES ; do
    sed -i -e "s/[ ]$node\$//g" $OURDIR/hosts.stripped
done
cat $OURDIR/hosts.stripped >> /etc/hosts.tmp
mv /etc/hosts.tmp /etc/hosts

# Some services (neutron-ovs-cleanup) might lookup the hostname prior to
# network being up.  We have to handle this here once at startup; then
# again later in the rc.hostnames hook below.
echo $ctlip $hostname >> /tmp/hosts.tmp
cat /etc/hosts >> /tmp/hosts.tmp
mv /tmp/hosts.tmp /etc/hosts

grep -q DYNRUNDIR /etc/emulab/paths.sh
if [ $? -eq 0 ]; then
    echo "*** Hooking Emulab rc.hostnames boot script..."
    mkdir -p $OURDIR/bin
    touch $OURDIR/bin/rc.hostnames-openstack
    chmod 755 $OURDIR/bin/rc.hostnames-openstack
    cat <<EOF >$OURDIR/bin/rc.hostnames-openstack
#!/bin/sh

cp -p $OURDIR/mgmt-hosts /var/run/emulab/hosts.head

# Some services (neutron-ovs-cleanup) might lookup the hostname prior to
# network being up.
echo $ctlip $hostname >> /var/run/emulab/hosts.head

exit 0
EOF

    mkdir -p /usr/local/etc/emulab/run/rcmanifest.d
    touch /usr/local/etc/emulab/run/rcmanifest.d/0.openstack-rcmanifest
    cat <<EOF >> /usr/local/etc/emulab/run/rcmanifest.d/0.openstack-rcmanifest
HOOK SERVICE=rc.hostnames ENV=boot WHENCE=every OP=boot POINT=pre FATAL=0 FILE=$OURDIR/bin/rc.hostnames-openstack ARGV="" 
EOF
else
    echo "*** Nullifying Emulab rc.hostnames and rc.ifconfig services!"
    mv /usr/local/etc/emulab/rc/rc.hostnames /usr/local/etc/emulab/rc/rc.hostnames.NO
    mv /usr/local/etc/emulab/rc/rc.ifconfig /usr/local/etc/emulab/rc/rc.ifconfig.NO
fi

touch $OURDIR/hostnames-done

logtend "hostnames"

exit 0
