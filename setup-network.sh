#!/bin/sh

set -x

if [ -z "$EUID" ]; then
    EUID=`id -u`
fi
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "`dirname $0`/setup-lib.sh"

if [ -f $OURDIR/network-done ]; then
    exit 0
fi

logtstart "network"

# Setup /etc/hosts to point to the mgmtlan.
$DIRNAME/setup-hostnames.sh 1> $OURDIR/setup-hostnames.log 2>&1

if [ "${ML2PLUGIN}" = "openvswitch" ]; then
    echo "*** Moving Interfaces into OpenVSwitch Bridges"
    $DIRNAME/setup-ovs-node.sh 1> $OURDIR/setup-ovs-node.log 2>&1
else
    echo "*** Setting up Linux Bridge static network configuration"
    $DIRNAME/setup-linuxbridge-node.sh 1> $OURDIR/setup-linuxbridge-node.log 2>&1
fi

touch $OURDIR/network-done

logtend "network"

exit 0
